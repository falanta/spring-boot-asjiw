package com.mii.poc.backend.asjiw.entity;

public class Submit {
String nama_lengkap;
String jenis_kelamin;
String no_id;
String tempat_lahir;
String tanggal_lahir;
String status_perkawinan;
String alamat;
String kota;
String provinsi;
String negara;
String kode_pos;
String no_hp;
String email;
String produk;
String uang_pertanggungan;
String premi;
String frekuensi_bayar;
String jangka_waktu_perlindungan;
String masa_bayar_premi;
String nama_lengkap_ahli_waris;
String jenis_kelamin_ahli_waris;
String tanggal_lahir_ahli_waris;
String hubungan;
String status;
String sumber_rekening;
String no_ref;
String tanggal;
String response;
String response_message;
String no_polis;

public Submit() {
	
}

public Submit(String nama_lengkap, String jenis_kelamin, String no_id, String tempat_lahir, String tanggal_lahir,
		String status_perkawinan, String alamat, String kota, String provinsi, String negara, String kode_pos,
		String no_hp, String email, String produk, String uang_pertanggungan, String premi, String frekuensi_bayar,
		String jangka_waktu_perlindungan, String masa_bayar_premi, String nama_lengkap_ahli_waris,
		String jenis_kelamin_ahli_waris, String tanggal_lahir_ahli_waris, String hubungan, String status,
		String sumber_rekening, String no_ref, String tanggal, String response, String response_message,String no_polis) {
	super();
	this.nama_lengkap = nama_lengkap;
	this.jenis_kelamin = jenis_kelamin;
	this.no_id = no_id;
	this.tempat_lahir = tempat_lahir;
	this.tanggal_lahir = tanggal_lahir;
	this.status_perkawinan = status_perkawinan;
	this.alamat = alamat;
	this.kota = kota;
	this.provinsi = provinsi;
	this.negara = negara;
	this.kode_pos = kode_pos;
	this.no_hp = no_hp;
	this.email = email;
	this.produk = produk;
	this.uang_pertanggungan = uang_pertanggungan;
	this.premi = premi;
	this.frekuensi_bayar = frekuensi_bayar;
	this.jangka_waktu_perlindungan = jangka_waktu_perlindungan;
	this.masa_bayar_premi = masa_bayar_premi;
	this.nama_lengkap_ahli_waris = nama_lengkap_ahli_waris;
	this.jenis_kelamin_ahli_waris = jenis_kelamin_ahli_waris;
	this.tanggal_lahir_ahli_waris = tanggal_lahir_ahli_waris;
	this.hubungan = hubungan;
	this.status = status;
	this.sumber_rekening = sumber_rekening;
	this.no_ref = no_ref;
	this.tanggal = tanggal;
	this.response = response;
	this.response_message = response_message;
	this.no_polis=no_polis;
}



public String getNo_polis() {
	return no_polis;
}

public void setNo_polis(String no_polis) {
	this.no_polis = no_polis;
}

public String getNama_lengkap() {
	return nama_lengkap;
}

public void setNama_lengkap(String nama_lengkap) {
	this.nama_lengkap = nama_lengkap;
}

public String getJenis_kelamin() {
	return jenis_kelamin;
}

public void setJenis_kelamin(String jenis_kelamin) {
	this.jenis_kelamin = jenis_kelamin;
}

public String getNo_id() {
	return no_id;
}

public void setNo_id(String no_id) {
	this.no_id = no_id;
}

public String getTempat_lahir() {
	return tempat_lahir;
}

public void setTempat_lahir(String tempat_lahir) {
	this.tempat_lahir = tempat_lahir;
}

public String getTanggal_lahir() {
	return tanggal_lahir;
}

public void setTanggal_lahir(String tanggal_lahir) {
	this.tanggal_lahir = tanggal_lahir;
}

public String getStatus_perkawinan() {
	return status_perkawinan;
}

public void setStatus_perkawinan(String status_perkawinan) {
	this.status_perkawinan = status_perkawinan;
}

public String getAlamat() {
	return alamat;
}

public void setAlamat(String alamat) {
	this.alamat = alamat;
}

public String getKota() {
	return kota;
}

public void setKota(String kota) {
	this.kota = kota;
}

public String getProvinsi() {
	return provinsi;
}

public void setProvinsi(String provinsi) {
	this.provinsi = provinsi;
}

public String getNegara() {
	return negara;
}

public void setNegara(String negara) {
	this.negara = negara;
}

public String getKode_pos() {
	return kode_pos;
}

public void setKode_pos(String kode_pos) {
	this.kode_pos = kode_pos;
}

public String getNo_hp() {
	return no_hp;
}

public void setNo_hp(String no_hp) {
	this.no_hp = no_hp;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public String getProduk() {
	return produk;
}

public void setProduk(String produk) {
	this.produk = produk;
}

public String getUang_pertanggungan() {
	return uang_pertanggungan;
}

public void setUang_pertanggungan(String uang_pertanggungan) {
	this.uang_pertanggungan = uang_pertanggungan;
}

public String getPremi() {
	return premi;
}

public void setPremi(String premi) {
	this.premi = premi;
}

public String getFrekuensi_bayar() {
	return frekuensi_bayar;
}

public void setFrekuensi_bayar(String frekuensi_bayar) {
	this.frekuensi_bayar = frekuensi_bayar;
}

public String getJangka_waktu_perlindungan() {
	return jangka_waktu_perlindungan;
}

public void setJangka_waktu_perlindungan(String jangka_waktu_perlindungan) {
	this.jangka_waktu_perlindungan = jangka_waktu_perlindungan;
}

public String getMasa_bayar_premi() {
	return masa_bayar_premi;
}

public void setMasa_bayar_premi(String masa_bayar_premi) {
	this.masa_bayar_premi = masa_bayar_premi;
}

public String getNama_lengkap_ahli_waris() {
	return nama_lengkap_ahli_waris;
}

public void setNama_lengkap_ahli_waris(String nama_lengkap_ahli_waris) {
	this.nama_lengkap_ahli_waris = nama_lengkap_ahli_waris;
}

public String getJenis_kelamin_ahli_waris() {
	return jenis_kelamin_ahli_waris;
}

public void setJenis_kelamin_ahli_waris(String jenis_kelamin_ahli_waris) {
	this.jenis_kelamin_ahli_waris = jenis_kelamin_ahli_waris;
}

public String getTanggal_lahir_ahli_waris() {
	return tanggal_lahir_ahli_waris;
}

public void setTanggal_lahir_ahli_waris(String tanggal_lahir_ahli_waris) {
	this.tanggal_lahir_ahli_waris = tanggal_lahir_ahli_waris;
}

public String getHubungan() {
	return hubungan;
}

public void setHubungan(String hubungan) {
	this.hubungan = hubungan;
}

public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}

public String getSumber_rekening() {
	return sumber_rekening;
}

public void setSumber_rekening(String sumber_rekening) {
	this.sumber_rekening = sumber_rekening;
}

public String getNo_ref() {
	return no_ref;
}

public void setNo_ref(String no_ref) {
	this.no_ref = no_ref;
}

public String getTanggal() {
	return tanggal;
}

public void setTanggal(String tanggal) {
	this.tanggal = tanggal;
}

public String getResponse() {
	return response;
}

public void setResponse(String response) {
	this.response = response;
}

public String getResponse_message() {
	return response_message;
}

public void setResponse_message(String response_message) {
	this.response_message = response_message;
}


	

	
}
