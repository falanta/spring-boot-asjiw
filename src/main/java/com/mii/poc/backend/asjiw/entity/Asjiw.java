package com.mii.poc.backend.asjiw.entity;


import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "log_trx")
public class Asjiw implements Serializable {
private static final long serialVersionUID = -2343243243242432341L;
@Id @GeneratedValue(generator="system-uuid")
@GenericGenerator(name="system-uuid", strategy = "uuid")
private String id;
//@Column(name = "name")
//private String name;
//Setters, getters and constructors

@Column(name = "status")	
private String status;
@Column(name = "produk_asuransi")
private String produk_asuransi;
@Column(name = "perusahaan")
private String perusahaan;
@Column(name = "no_polis")
private String no_polis;
@Column(name = "uang_pertanggungan")
private String uang_pertanggungan;
@Column(name = "premi")
private String premi;
@Column(name = "frekuensi_bayar")
private String frekuensi_bayar;
@Column(name = "jangka_waktu_perlindungan")
private String jangka_waktu_perlindungan;
@Column(name = "masa_bayar_premi")
private String masa_bayar_premi;
@Column(name = "tanggal_bayar")
private String tanggal_bayar;

public Asjiw() {
}

public Asjiw(String status, String produk_asuransi, String perusahaan, String no_polis, String uang_pertanggungan,
		String premi, String frekuensi_bayar, String jangka_waktu_perlindungan, String masa_bayar_premi,
		String tanggal_bayar) {
	super();
	this.status = status;
	this.produk_asuransi = produk_asuransi;
	this.perusahaan = perusahaan;
	this.no_polis = no_polis;
	this.uang_pertanggungan = uang_pertanggungan;
	this.premi = premi;
	this.frekuensi_bayar = frekuensi_bayar;
	this.jangka_waktu_perlindungan = jangka_waktu_perlindungan;
	this.masa_bayar_premi = masa_bayar_premi;
	this.tanggal_bayar = tanggal_bayar;
}

public String getId() {
	return id;
}

public void setId(String id) {
	this.id = id;
}

public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}

public String getProduk_asuransi() {
	return produk_asuransi;
}

public void setProduk_asuransi(String produk_asuransi) {
	this.produk_asuransi = produk_asuransi;
}

public String getPerusahaan() {
	return perusahaan;
}

public void setPerusahaan(String perusahaan) {
	this.perusahaan = perusahaan;
}

public String getNo_polis() {
	return no_polis;
}

public void setNo_polis(String no_polis) {
	this.no_polis = no_polis;
}

public String getUang_pertanggungan() {
	return uang_pertanggungan;
}

public void setUang_pertanggungan(String uang_pertanggungan) {
	this.uang_pertanggungan = uang_pertanggungan;
}

public String getPremi() {
	return premi;
}

public void setPremi(String premi) {
	this.premi = premi;
}

public String getFrekuensi_bayar() {
	return frekuensi_bayar;
}

public void setFrekuensi_bayar(String frekuensi_bayar) {
	this.frekuensi_bayar = frekuensi_bayar;
}

public String getJangka_waktu_perlindungan() {
	return jangka_waktu_perlindungan;
}

public void setJangka_waktu_perlindungan(String jangka_waktu_perlindungan) {
	this.jangka_waktu_perlindungan = jangka_waktu_perlindungan;
}

public String getMasa_bayar_premi() {
	return masa_bayar_premi;
}

public void setMasa_bayar_premi(String masa_bayar_premi) {
	this.masa_bayar_premi = masa_bayar_premi;
}

public String getTanggal_bayar() {
	return tanggal_bayar;
}

public void setTanggal_bayar(String tanggal_bayar) {
	this.tanggal_bayar = tanggal_bayar;
}

public static long getSerialversionuid() {
	return serialVersionUID;
}




}