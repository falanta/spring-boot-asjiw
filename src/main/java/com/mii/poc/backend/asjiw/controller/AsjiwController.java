package com.mii.poc.backend.asjiw.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mii.poc.backend.asjiw.entity.Asjiw;
import com.mii.poc.backend.asjiw.entity.EntityUI;
import com.mii.poc.backend.asjiw.entity.Submit;
import com.mii.poc.backend.asjiw.kafka.Consumer;
import com.mii.poc.backend.asjiw.kafka.Producer;
//import com.mii.poc.backend.asjiw.repository.AsjiwRepository;

@RestController
public class AsjiwController {
//	@Autowired
//	AsjiwRepository repository;
	
	
	
	@CrossOrigin
	@GetMapping("/cari")
	public String cari() throws IOException {
// save a single Account
		
		
		String response="{\r\n" + 
				"	\"asuransi_1\":\r\n" + 
				"	{\r\n" + 
				"		\"nama_asuransi\":\"PRIMAJAGA 100\",\r\n" + 
				"		\"premi\":\"Premi mulai dari IDR 10.000 - IDR 1.000.000\",\r\n" + 
				"		\"jangka_waktu_perlindungan\":\"3-8 tahun\",\r\n" + 
				"		\"manfaat_bulanan \":\"20x premi bulanan selama sisa masa pertanggungan\",\r\n" + 
				"		\"manfaat_dibayarkan \":\"secara sekaligus (lumpsum) sebesar 400x premi bulanan\",\r\n" + 
				"		\"biaya\":\"IDR. 50.000\",\r\n" + 
				"		\"tipe_pembayaran\":\"per bulan\"\r\n" + 
				"	},\r\n" + 
				"	\r\n" + 
				"	\"asuransi_2\":\r\n" + 
				"	{\r\n" + 
				"		\"nama_asuransi\":\"PRIMAJAGA 100\",\r\n" + 
				"		\"premi\":\"Premi mulai dari IDR 10.000 - IDR 1.000.000\",\r\n" + 
				"		\"jangka_waktu_perlindungan\":\"3-8 tahun\",\r\n" + 
				"		\"manfaat_bulanan \":\"20x premi bulanan selama sisa masa pertanggungan\",\r\n" + 
				"		\"manfaat_dibayarkan \":\"secara sekaligus (lumpsum) sebesar 400x premi bulanan\",\r\n" + 
				"		\"biaya\":\"IDR. 100.000\",\r\n" + 
				"		\"tipe_pembayaran\":\"per bulan\"\r\n" + 
				"	},\r\n" + 
				"	\r\n" + 
				"	\"asuransi_3\":\r\n" + 
				"	{\r\n" + 
				"		\"nama_asuransi\":\"PRIMAJAGA 100\",\r\n" + 
				"		\"premi\":\"Premi mulai dari IDR 10.000 - IDR 1.000.000\",\r\n" + 
				"		\"jangka_waktu_perlindungan\":\"3-8 tahun\",\r\n" + 
				"		\"manfaat_bulanan \":\"20x premi bulanan selama sisa masa pertanggungan\",\r\n" + 
				"		\"manfaat_dibayarkan \":\"secara sekaligus (lumpsum) sebesar 400x premi bulanan\",\r\n" + 
				"		\"biaya\":\"IDR. 150.000\",\r\n" + 
				"		\"tipe_pembayaran\":\"per bulan\"\r\n" + 
				"	},\r\n" + 
				"	\r\n" + 
				"	\"asuransi_4\":\r\n" + 
				"	{\r\n" + 
				"		\"nama_asuransi\":\"PRIMAJAGA 100\",\r\n" + 
				"		\"premi\":\"Premi mulai dari IDR 10.000 - IDR 1.000.000\",\r\n" + 
				"		\"jangka_waktu_perlindungan\":\"3-8 tahun\",\r\n" + 
				"		\"manfaat_bulanan \":\"20x premi bulanan selama sisa masa pertanggungan\",\r\n" + 
				"		\"manfaat_dibayarkan \":\"secara sekaligus (lumpsum) sebesar 400x premi bulanan\",\r\n" + 
				"		\"biaya\":\"IDR. 200.000\",\r\n" + 
				"		\"tipe_pembayaran\":\"per bulan\"\r\n" + 
				"	}\r\n" + 
				"}";
		
		
		return "{\r\n" + 
				"	\"asuransi_1\":\r\n" + 
				"	{\r\n" + 
				"		\"nama_asuransi\":\"PRIMAJAGA 100\",\r\n" + 
				"		\"premi\":\"Premi mulai dari IDR 10.000 - IDR 1.000.000\",\r\n" + 
				"		\"jangka_waktu_perlindungan\":\"3-8 tahun\",\r\n" + 
				"		\"manfaat_bulanan \":\"20x premi bulanan selama sisa masa pertanggungan\",\r\n" + 
				"		\"manfaat_dibayarkan \":\"secara sekaligus (lumpsum) sebesar 400x premi bulanan\",\r\n" + 
				"		\"biaya\":\"IDR. 50.000\",\r\n" + 
				"		\"tipe_pembayaran\":\"per bulan\"\r\n" + 
				"	},\r\n" + 
				"	\r\n" + 
				"	\"asuransi_2\":\r\n" + 
				"	{\r\n" + 
				"		\"nama_asuransi\":\"PRIMAJAGA 100\",\r\n" + 
				"		\"premi\":\"Premi mulai dari IDR 10.000 - IDR 1.000.000\",\r\n" + 
				"		\"jangka_waktu_perlindungan\":\"3-8 tahun\",\r\n" + 
				"		\"manfaat_bulanan \":\"20x premi bulanan selama sisa masa pertanggungan\",\r\n" + 
				"		\"manfaat_dibayarkan \":\"secara sekaligus (lumpsum) sebesar 400x premi bulanan\",\r\n" + 
				"		\"biaya\":\"IDR. 100.000\",\r\n" + 
				"		\"tipe_pembayaran\":\"per bulan\"\r\n" + 
				"	},\r\n" + 
				"	\r\n" + 
				"	\"asuransi_3\":\r\n" + 
				"	{\r\n" + 
				"		\"nama_asuransi\":\"PRIMAJAGA 100\",\r\n" + 
				"		\"premi\":\"Premi mulai dari IDR 10.000 - IDR 1.000.000\",\r\n" + 
				"		\"jangka_waktu_perlindungan\":\"3-8 tahun\",\r\n" + 
				"		\"manfaat_bulanan \":\"20x premi bulanan selama sisa masa pertanggungan\",\r\n" + 
				"		\"manfaat_dibayarkan \":\"secara sekaligus (lumpsum) sebesar 400x premi bulanan\",\r\n" + 
				"		\"biaya\":\"IDR. 150.000\",\r\n" + 
				"		\"tipe_pembayaran\":\"per bulan\"\r\n" + 
				"	},\r\n" + 
				"	\r\n" + 
				"	\"asuransi_4\":\r\n" + 
				"	{\r\n" + 
				"		\"nama_asuransi\":\"PRIMAJAGA 100\",\r\n" + 
				"		\"premi\":\"Premi mulai dari IDR 10.000 - IDR 1.000.000\",\r\n" + 
				"		\"jangka_waktu_perlindungan\":\"3-8 tahun\",\r\n" + 
				"		\"manfaat_bulanan \":\"20x premi bulanan selama sisa masa pertanggungan\",\r\n" + 
				"		\"manfaat_dibayarkan \":\"secara sekaligus (lumpsum) sebesar 400x premi bulanan\",\r\n" + 
				"		\"biaya\":\"IDR. 200.000\",\r\n" + 
				"		\"tipe_pembayaran\":\"per bulan\"\r\n" + 
				"	}\r\n" + 
				"}";
	}
	
	@CrossOrigin
	@GetMapping("/detail")
	public String detail() {
// save a single Account
		
		return "{\r\n" + 
				"	\"manfaat\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua -Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. -Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. -Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.-Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.-Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.\",\r\n" + 
				"	\"syarat_ketentuan\": \"-Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. -Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. -Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. -Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"\r\n" + 
				"	\"klaim\": \"-Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. -Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. -Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. -Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.-Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. -Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. -Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. -Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Premi 100.000/bulan\"\r\n" + 
				"	\"manfaat_footer\":\"Ringkasan Informasi Produk | F.A.Q | Brosur\",\r\n" + 
				"	\"syarat_ketentuan_footer\":\"Syarat & Ketentuan Lengkap\",\r\n" + 
				"	\"klaim_footer\":\"Hello Danamon: 1-500-090\",\r\n" + 
				"	\"premi\":\"IDR25.000 per bulan\"\r\n" + 
				"}";
	}
	
	@CrossOrigin
	@GetMapping("/data_peserta")
	public String data_peserta() {
// save a single Account
		
		return "{\r\n" + 
				"	\"nama_lengkap\":\"Agus Setiawan\",\r\n" + 
				"	\"jenis_kelamin\":\"Pria\",\r\n" + 
				"	\"no_id\":\"1234123412341234\",\r\n" + 
				"	\"tempat_lahir\":\"Jakarta\",\r\n" + 
				"	\"tanggal_lahir\":\"20/03/1997\",\r\n" + 
				"	\"status_perkawinan\":\"Belum Kawin\",\r\n" + 
				"	\"alamat\":\"Jl. HR Rasuna Said Blok C no.10, Karet Kuningan, Setiabudi\",\r\n" + 
				"	\"kota\":\"Jakarta Selatan\",\r\n" + 
				"	\"provinsi\":\"DKI Jakarta\",\r\n" + 
				"	\"negara\":\"Indonesia\",\r\n" + 
				"	\"kode_Pos\":\"12910\",\r\n" + 
				"	\"no_hp\":\"08157777112\",\r\n" + 
				"	\"email\":\"agus.setiawan@gmail.com\"\r\n" + 
				"}";
	}
	
	@CrossOrigin
	@GetMapping("/detail_asuransi")
	public String detail_asuransi() {
// save a single Account
		
		return "{\r\n" + 
				"	\"produk\":\"PRIMAJAGA 100\",\r\n" + 
				"	\"uang_pertanggungan\":\"IDR 5.000.000,00\",\r\n" + 
				"	\"premi\":\"IDR 25.000,00\",\r\n" + 
				"	\"frekuensi_bayar\":\"Bulanan\",\r\n" + 
				"	\"jangka_waktu_perlindungan\":\"10 tahun\",\r\n" + 
				"	\"masa_bayar_premi\":\"10 tahun\"\r\n" + 
				"}";
	}
	
	@CrossOrigin
	@GetMapping("/informasi")
	public String informasi() {
// save a single Account
		
		return "{\r\n" + 
				"	\"no_referensi\": \"12345678901234001\",\r\n" + 
				"	\"tanggal\": \"16/12/2019 14:01\",\r\n" + 
				"	\"sumber_rekening\": \"tabungan Danamon Lebih 0035981199203 IDR\",\r\n" + 
				"	\"produk_asuransi\": \"Primajaga 100\",\r\n" + 
				"	\"uang_pertanggungan\": \"IDR 5.000.000,00\",\r\n" + 
				"	\"premi\": \"IDR 25.000,00\",\r\n" + 
				"	\"frekuensi_bayar\": \"bulanan\",\r\n" + 
				"	\"jangka_waktu_perlindungan\": \"10 tahun\",\r\n" + 
				"	\"masa_bayar_premi\": \"10 tahun\"\r\n" + 
				"}";
	}
	
	@CrossOrigin
	@GetMapping("/detail_status")
	public String detail_status() {
// save a single Account
		
		return "{\r\n" + 
				"\"status\": \"Berhasil/ Polis sudah terbit\",\r\n" + 
				"\"produk_asuransi\": \"Primajaga 100\",\r\n" + 
				"\"perusahaan\": \"Manulife\",\r\n" + 
				"\"no_polis\": \"1234567890123456\",\r\n" + 
				"\"uang_Pertanggungan\"=\"IDR 5.000.000,00\",\r\n" + 
				"\"premi\": \"IDR 25.000,00\",\r\n" + 
				"\"frekuensi_bayar\": \"Bulanan\",\r\n" + 
				"\"jangka_waktu_perlindungan\": \"10 tahun\",\r\n" + 
				"\"masa_bayar_premi\": \"10 tahun\"\r\n" + 
				"}";
	}
	
	
	@Autowired
    private KafkaTemplate<String, Submit> kafkaTemplate;
	private static final String TOPIC = "users";
	
	@CrossOrigin
	@RequestMapping(value = "/kirim", method = RequestMethod.POST)
    public String kirim(
    		@RequestParam("nama_lengkap") String nama_lengkap,
    		@RequestParam("jenis_kelamin") String jenis_kelamin,
    		@RequestParam("no_id") String no_id,
    		@RequestParam("tempat_lahir") String tempat_lahir,
    		@RequestParam("tanggal_lahir") String tanggal_lahir,
    		@RequestParam("status_perkawinan") String status_perkawinan,
    		@RequestParam("alamat") String alamat,
    		@RequestParam("kota") String kota,
    		@RequestParam("provinsi") String provinsi,
    		@RequestParam("negara") String negara,
    		@RequestParam("kode_pos") String kode_pos,
    		@RequestParam("no_hp") String no_hp,
    		@RequestParam("email") String email,
    		@RequestParam("produk") String produk,
    		@RequestParam("uang_pertanggungan") String uang_pertanggungan,
    		@RequestParam("premi") String premi,
    		@RequestParam("frekuensi_bayar") String frekuensi_bayar,
    		@RequestParam("jangka_waktu_perlindungan") String jangka_waktu_perlindungan,
    		@RequestParam("masa_bayar_premi") String masa_bayar_premi,
    		@RequestParam("nama_lengkap_ahli_waris") String nama_lengkap_ahli_waris,
    		@RequestParam("jenis_kelamin_ahli_waris") String jenis_kelamin_ahli_waris,
    		@RequestParam("tanggal_lahir_ahli_waris") String tanggal_lahir_ahli_waris,
    		@RequestParam("hubungan") String hubungan,
    		@RequestParam("sumber_rekening") String sumber_rekening
    		) throws Exception     
{
 
	
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		String no_ref="123456789";
		String no_polis="AB123123456123POL";
		String status="Berhasil/Polis Sudah Terbit";
		String tanggal=formatter.format(date);
		String response="Pembelian sedang di proses.";
		String response_message="Selamat, Anda telah mengirimkan data anda sebagai untuk pemesanan Asuransi Mobil. Tim kami akan menghubungi Anda.";
		Submit submit=new Submit(nama_lengkap, jenis_kelamin, no_id, tempat_lahir, tanggal_lahir,
				status_perkawinan, alamat, kota, provinsi, negara, kode_pos,
				no_hp, email, produk, uang_pertanggungan, premi, frekuensi_bayar,
				jangka_waktu_perlindungan, masa_bayar_premi, nama_lengkap_ahli_waris,
				jenis_kelamin_ahli_waris, tanggal_lahir_ahli_waris, hubungan, status,
				sumber_rekening, no_ref, tanggal, response, response_message,no_polis) ;
		
		 ObjectMapper mapper = new ObjectMapper();
		 String jsonString = mapper.writeValueAsString(submit);
		 
		 this.kafkaTemplate.send(TOPIC, submit);
		
  return jsonString;
}
	
	@CrossOrigin
	@GetMapping("/konfirmasi")
	public String konfirmasi() {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		
		String status="Berhasil/ Polis sudah terbit";
		String produk_asuransi="Primajaga 100";
		String perusahaan="Manulife";
		String no_polis="1234567890123456";
		String uang_pertanggungan="IDR 5.000.000,00";
		String premi="IDR 25.000,00";
		String frekuensi_bayar="Bulanan";
		String jangka_waktu_perlindungan="10 tahun";
		String masa_bayar_premi="10 tahun";
		String tanggal_bayar=formatter.format(date);
	//	repository.save(new Asjiw(status,produk_asuransi,perusahaan,no_polis,uang_pertanggungan,premi,frekuensi_bayar,jangka_waktu_perlindungan,masa_bayar_premi,tanggal_bayar));
		
		
		return "{\r\n" + 
				"    \"status\": \""+status+"\",\r\n" + 
				"	\"no_polis\": \""+no_polis+"\"\r\n" + 
				"}";
	}
	
	
	

}