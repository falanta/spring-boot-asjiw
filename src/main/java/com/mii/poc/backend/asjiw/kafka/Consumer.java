package com.mii.poc.backend.asjiw.kafka;

import java.io.IOException;

import org.springframework.kafka.annotation.KafkaListener;

public class Consumer {
	
//	 private final Logger logger = LoggerFactory.getLogger(Producer.class);

	    @KafkaListener(topics = "users", groupId = "group_id")
	    public void consume(String message) throws IOException {
	        //logger.info(String.format("#### -> Consumed message -> %s", message));
	        System.out.println("#### -> Consumed message -> %s "+message);
	    }
	
}
