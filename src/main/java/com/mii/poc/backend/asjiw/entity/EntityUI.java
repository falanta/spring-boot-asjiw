package com.mii.poc.backend.asjiw.entity;

public class EntityUI  {
 
		
	private String id;
     private String name;
     protected EntityUI() {
	}
 
	public EntityUI(String id, String name) {
		this.id = id;
		this.name = name;
	}
 
	 public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
	
	public String toString() {
		return String.format("Account[id='%s', name='%s']", id, name);
	}
}
